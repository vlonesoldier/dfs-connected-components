#include <iostream>
#include <vector>

using namespace std;

struct wierzcholki
{
    vector<int> A;

    bool odwiedzony;
} *v;

void DFS(int a)
{
    v[a].odwiedzony = true;

    for (int i = 0; i < v[a].A.size(); i++)
    {
        if (!v[v[a].A[i]].odwiedzony)
            DFS(v[a].A[i]);
    }
}

void polaczenia(int n)
{

    for (int i = 0; i < n; i++)
    {
        v[i].odwiedzony = false;
    }

    int liczba = 0;

    for (int i = 0; i < n; i++)
    {
        if (v[i].odwiedzony == false)
        {
            DFS(i);
            liczba++;
        }
    }
    cout << "Liczba skladowych: " << liczba;
}

int main()
{
    unsigned int n, k, a, b;
    cin >> n;
    cin >> k;

    v = new wierzcholki[n + 1];

    for (int i = 0; i < k; i++)
    {
        cin >> a >> b;

        v[a].A.push_back(b);
        v[b].A.push_back(a);
    }

    polaczenia(n);

    delete[] v;

    return 0;
}
