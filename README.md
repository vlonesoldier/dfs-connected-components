# DFS - connected components
This program tells you how many connected components specified graph has

It's based on DFS algorithm

Symbols:

n - vertices' amount\
k - edges' amount\
a - number of vertex (starting from 0)\
b - number of vertex to make connection with the previous one


## Authors

- [@vlonesoldier](https://gitlab.com/vlonesoldier)
